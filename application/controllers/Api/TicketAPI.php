<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TicketAPI extends CI_Controller {

	 function __construct() {
        parent::__construct();

        // LIBRARY
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('form_validation');

        // HELPER
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');

        // MODEL
        $this->load->model('Users_Model');
        $this->load->model('Generate_Model');
        $this->load->model('Email_Model');
        $this->load->model('Tickets_Model');
        $this->load->model('Projects_Model');
        $this->load->model('Log_Model');
    }

    public function index() {
        echo "string";
    }

    public function telegram() {
        $reply_message = '';

        $explText = explode(' ', $this->input->get('text'));
        $cekProject = $this->Projects_Model->project_by_idproject($explText[0]);
        if(count($cekProject) > 0) {
            foreach ($cekProject as $key => $value) {

                // CEK TIKET
                $cekTiketProject = $this->Tickets_Model->ticket_by_project($explText[0]);
                if(count($cekTiketProject) > 0) {

                    foreach ($cekTiketProject as $k => $v) {
                        if($v->ticket_status == 'close') {
                            $reply_message .= 'Your ticket have close';
                        } else {

                            if($this->input->get('from_id') == $v->ticket_from_id) {
                                // CHAT
                                $id_chat                 = $this->Generate_Model->generate_id_user();
                                $id_ticket_              = $v->id_ticket;
                                $id_project_             = $value->id_project;
                                $chat_user_role_         = 'client';
                                $chat_id_user_           = $this->input->get('from_id');
                                $chat_platform_          = 'telegram';
                                $chat_field_             = $this->input->get('text');
                                $chat_file               = '';


                                // FILEEEEE
                                
                                $rand = rand($this->input->get('update_id'), $this->input->get('from_id'));

                                if($this->input->get('document') != '') {
                                    $explDoc = explode('/', $this->input->get('document'));
                                    $lastArr = count($explDoc) - 1;

                                    $fileUrl = file_get_contents($this->input->get('document'));
                                    file_put_contents("files/telegrams/".$rand."-".$explDoc[$lastArr], $fileUrl);
                                    
                                    $chat_file = "files/telegrams/".$rand."-".$explDoc[$lastArr];
                                }

                                if($this->input->get('photoname') != '') {
                                    $explDoc = explode('/', $this->input->get('photoname'));
                                    $lastArr = count($explDoc) - 1;

                                    $fileUrl = file_get_contents($this->input->get('photoname'));
                                    file_put_contents("files/telegrams/".$rand."-".$explDoc[$lastArr], $fileUrl);

                                    $chat_file = "files/telegrams/".$rand."-".$explDoc[$lastArr];
                                }

                                $this->Tickets_Model->insert_chat($id_chat, $id_ticket_, $id_project_, $chat_user_role_, $chat_id_user_, $chat_platform_, $chat_field_, $chat_file);
                            } else {
                                $reply_message .= 'Someone from your team is talking or in the queue to be handled by our support';
                            }
                        }
                    }
                } else {
                    // INSERT TICKET
                    $id_ticket              = $this->Generate_Model->generate_id_user();
                    $id_project             = $value->id_project;
                    $ticket_desc            = $this->input->get('text');
                    $ticket_priority        = 'general_issue';
                    $ticket_from_platform   = 'telegram';
                    $ticket_from_id         = $this->input->get('from_id');
                    $ticket_status          = 'open';

                    $save = $this->Tickets_Model->ticket_save($id_ticket, $id_project, $ticket_desc, $ticket_priority, $ticket_from_platform, $ticket_from_id, $ticket_status);
                    if($save) {

                        // CHAT
                        $id_chat                 = $this->Generate_Model->generate_id_user();
                        $id_ticket_              = $id_ticket;
                        $id_project_             = $id_project;
                        $chat_user_role_         = 'client';
                        $chat_id_user_           = $this->input->get('from_id');
                        $chat_platform_          = 'telegram';
                        $chat_field_             = $this->input->get('text');
                        $chat_file               = '';


                        // FILEEEEE
                        
                        $rand = rand($this->input->get('update_id'), $this->input->get('from_id'));

                        if($this->input->get('document') != '') {
                            $explDoc = explode('/', $this->input->get('document'));
                            $lastArr = count($explDoc) - 1;

                            $fileUrl = file_get_contents($this->input->get('document'));
                            file_put_contents("files/telegrams/".$rand."-".$explDoc[$lastArr], $fileUrl);
                            
                            $chat_file = "files/telegrams/".$rand."-".$explDoc[$lastArr];
                        }

                        if($this->input->get('photoname') != '') {
                            $explDoc = explode('/', $this->input->get('photoname'));
                            $lastArr = count($explDoc) - 1;

                            $fileUrl = file_get_contents($this->input->get('photoname'));
                            file_put_contents("files/telegrams/".$rand."-".$explDoc[$lastArr], $fileUrl);

                            $chat_file = "files/telegrams/".$rand."-".$explDoc[$lastArr];
                        }

                        $saveChats = $this->Tickets_Model->insert_chat($id_chat, $id_ticket_, $id_project_, $chat_user_role_, $chat_id_user_, $chat_platform_, $chat_field_, $chat_file);

                        if($saveChats) {
                            $reply_message .= 'Thanks, Your ticket number is '.$id_ticket;
                        } else {
                            $reply_message .= 'Sorry, Message Send Failed';
                        }
                    } else {
                        $reply_message .= 'Sorry, Message Save Failed';
                    }
                }
            }
        } else {
            if($this->input->get('text') == '/start') {
                $reply_message .= 'Hi, for create ticket, type PROJECT_ID <space> Describer what you want for support help';
            } else {
                $cekTicketSession = $this->Tickets_Model->cek_ticket_session('telegram', $this->input->get('from_id'));
                if(count($cekTicketSession) > 0) {
                    foreach ($cekTicketSession as $k => $v) {
                        if($v->ticket_status == 'close') {
                            $reply_message .= 'Your ticket have close at '.$v->close_datetime.', for create ticket, type PROJECT_ID <space> Describer what you want for support help';
                        } else {
                            if($this->input->get('from_id') == $v->ticket_from_id) {
                                // CHAT
                                $id_chat                 = $this->Generate_Model->generate_id_user();
                                $id_ticket_              = $v->id_ticket;
                                $id_project_             = $v->id_project;
                                $chat_user_role_         = 'client';
                                $chat_id_user_           = $this->input->get('from_id');
                                $chat_platform_          = 'telegram';
                                $chat_field_             = $this->input->get('text');
                                $chat_file               = '';


                                // FILEEEEE

                                $rand = rand($this->input->get('update_id'), $this->input->get('from_id'));

                                if($this->input->get('document') != '') {
                                    $explDoc = explode('/', $this->input->get('document'));
                                    $lastArr = count($explDoc) - 1;

                                    $fileUrl = file_get_contents($this->input->get('document'));
                                    file_put_contents("files/telegrams/".$rand."-".$explDoc[$lastArr], $fileUrl);

                                    $chat_file = "files/telegrams/".$rand."-".$explDoc[$lastArr];
                                }

                                if($this->input->get('photoname') != '') {
                                    $explDoc = explode('/', $this->input->get('photoname'));
                                    $lastArr = count($explDoc) - 1;

                                    $fileUrl = file_get_contents($this->input->get('photoname'));
                                    file_put_contents("files/telegrams/".$rand."-".$explDoc[$lastArr], $fileUrl);

                                    $chat_file = "files/telegrams/".$rand."-".$explDoc[$lastArr];
                                }


                                $saveChats = $this->Tickets_Model->insert_chat($id_chat, $id_ticket_, $id_project_, $chat_user_role_, $chat_id_user_, $chat_platform_, $chat_field_, $chat_file);
                            } else {
                                $reply_message .= 'Someone from your team is talking or in the queue to be handled by our support';
                            }
                        }
                    }
                } else {
                    $reply_message .= 'Your project ID not found';
                }
            }
        }

        if($reply_message != '') {
            $sendMessageURL = "https://api.telegram.org/bot857032687:AAHAMmW_EydGDsTEFIv46pA8ymBixAFN8WQ/sendMessage?text=".urlencode($reply_message)."&chat_id=".$this->input->get('from_id');

            $curlSendMessage = curl_init();
            curl_setopt_array($curlSendMessage, array(
                CURLOPT_URL => $sendMessageURL,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Postman-Token: 7116f29b-4d05-4497-9c03-b9fad7afe299",
                    "cache-control: no-cache"
                ),
            ));

            curl_exec($curlSendMessage);
            curl_close($curlSendMessage);
        }
    }


    public function email() {
        $reply_message = '';

        $explText = explode('--,,--', $this->input->get('text'));

        if(strtolower(substr($explText[0], 0,2)) == 're') {
            $explSubject = explode(' ', $explText[0]);
            if(is_numeric($explSubject[1])) {
                $projectIdMail = $explSubject[1];
            } else {
                $reply_message .= 'For create ticket, type in your email subject PROJECT_ID <space> short describer what you want for support help';
            }
        } else {
            $explSubject = explode(' ', $explText[0]);

            if(is_numeric($explSubject[0])) {
                $projectIdMail = $explSubject[0];
            } else {
                $reply_message .= 'Hi, for create ticket, type in your email subject PROJECT_ID <space> short describer what you want for support help';
            }
        }

        if(isset($projectIdMail)) {
            $cekProject = $this->Projects_Model->project_by_idproject($projectIdMail);
            if(count($cekProject) > 0) {
                foreach ($cekProject as $key => $value) {

                    // CEK TIKET
                    $cekTiketProject = $this->Tickets_Model->ticket_by_project($projectIdMail);
                    if(count($cekTiketProject) > 0) {

                        foreach ($cekTiketProject as $k => $v) {
                            if($v->ticket_status == 'close') {
                                $reply_message .= 'Your mail ticket have close';
                            } else {

                                $explEmailAddr = explode(';', $this->input->get('from_id'));

                                $emailFrom = $explEmailAddr[0];
                                $emailCc = $explEmailAddr[1];

                                // URL
                                $explResExplEmailAddr = explode(',', $explEmailAddr[0]);
                                $from_email = $explResExplEmailAddr[0];

                                // DB
                                $explDBExplEmailAddr = explode(',', $v->ticket_from_id);
                                $db_email = $explDBExplEmailAddr[0];

                                if($from_email == $db_email) {

                                    $checkTicketLink = $this->Tickets_Model->cek_ticket_like($explText[0]);
                                    if(count($checkTicketLink) == 0) {
                                        // CHAT SUBJECT
                                        $id_chat                 = $this->Generate_Model->generate_id_user();
                                        $id_ticket_              = $v->id_ticket;
                                        $id_project_             = $value->id_project;
                                        $chat_user_role_         = 'client';
                                        $chat_id_user_           = $from_email;
                                        $chat_platform_          = 'email';
                                        $chat_field_             = $explText[0];
                                        $chat_file               = '';

                                        $this->Tickets_Model->insert_chat($id_chat, $id_ticket_, $id_project_, $chat_user_role_, $chat_id_user_, $chat_platform_, $chat_field_, $chat_file);

                                        // $reply_message .= "295";
                                    }
                                    
                                    // CHAT BODY
                                    $id_chat_bdy                = $this->Generate_Model->generate_id_user();
                                    $id_ticket_bdy              = $v->id_ticket;
                                    $id_project_bdy             = $value->id_project;
                                    $chat_user_role_bdy         = 'client';
                                    $chat_id_user_bdy           = $from_email;
                                    $chat_platform_bdy          = 'email';
                                    $chat_field_bdy             = $explText[1];
                                    $chat_file_bdy              = '';


                                    // FILEEEEE
                                    
                                    $rand = rand($this->input->get('update_id'), date('YmdHis'));

                                    if($this->input->get('document') != '') {
                                        $explDoc = explode('/', $this->input->get('document'));
                                        $lastArr = count($explDoc) - 1;

                                        $fileUrl = file_get_contents($this->input->get('document'));
                                        file_put_contents("files/emails/".$rand."-".$explDoc[$lastArr], $fileUrl);
                                        
                                        $chat_file_bdy = "files/emails/".$rand."-".$explDoc[$lastArr];
                                    }

                                    if($this->input->get('photoname') != '') {
                                        $explDoc = explode('/', $this->input->get('photoname'));
                                        $lastArr = count($explDoc) - 1;

                                        $fileUrl = file_get_contents($this->input->get('photoname'));
                                        file_put_contents("files/emails/".$rand."-".$explDoc[$lastArr], $fileUrl);

                                        $chat_file_bdy = "files/emails/".$rand."-".$explDoc[$lastArr];
                                    }

                                    $this->Tickets_Model->insert_chat($id_chat_bdy, $id_ticket_bdy, $id_project_bdy, $chat_user_role_bdy, $chat_id_user_bdy, $chat_platform_bdy, $chat_field_bdy, $chat_file_bdy);

                                    // $reply_message .= "335";
                                    
                                } else {
                                    $reply_message .= 'Someone from your team is talking or in the queue to be handled by our support email';
                                }
                            }
                        }
                    } else {

                        $explEmailAddr = explode(';', $this->input->get('from_id'));

                        $emailFrom = $explEmailAddr[0];
                        $emailCc = $explEmailAddr[1];

                        // URL
                        $explResExplEmailAddr = explode(',', $explEmailAddr[0]);
                        $from_email = $explResExplEmailAddr[0];

                        // INSERT TICKET
                        $id_ticket              = $this->Generate_Model->generate_id_user();
                        $id_project             = $value->id_project;
                        $ticket_desc            = $explText[0];
                        $ticket_priority        = 'general_issue';
                        $ticket_from_platform   = 'email';
                        $ticket_from_id         = $from_email;
                        $ticket_status          = 'open';

                        $save = $this->Tickets_Model->ticket_save($id_ticket, $id_project, $ticket_desc, $ticket_priority, $ticket_from_platform, $ticket_from_id, $ticket_status);
                        if($save) {
                            $checkTicketLink = $this->Tickets_Model->cek_ticket_like($explText[0]);
                            if(count($checkTicketLink) == 0) {
                                // CHAT SUBJECT
                                $id_chat                 = $this->Generate_Model->generate_id_user();
                                $id_ticket_              = $id_ticket;
                                $id_project_             = $value->id_project;
                                $chat_user_role_         = 'client';
                                $chat_id_user_           = $from_email;
                                $chat_platform_          = 'email';
                                $chat_field_             = $explText[0];
                                $chat_file               = '';

                                $this->Tickets_Model->insert_chat($id_chat, $id_ticket_, $id_project_, $chat_user_role_, $chat_id_user_, $chat_platform_, $chat_field_, $chat_file);

                                // $reply_message .= "380";
                            }
                            
                            // CHAT BODY
                            $id_chat_bdy                = $this->Generate_Model->generate_id_user();
                            $id_ticket_bdy              = $id_ticket;
                            $id_project_bdy             = $value->id_project;
                            $chat_user_role_bdy         = 'client';
                            $chat_id_user_bdy           = $from_email;
                            $chat_platform_bdy          = 'email';
                            $chat_field_bdy             = $explText[1];
                            $chat_file_bdy              = '';


                            // FILEEEEE
                            
                            $rand = rand($this->input->get('update_id'), date('YmdHis'));

                            if($this->input->get('document') != '') {
                                $explDoc = explode('/', $this->input->get('document'));
                                $lastArr = count($explDoc) - 1;

                                $fileUrl = file_get_contents($this->input->get('document'));
                                file_put_contents("files/emails/".$rand."-".$explDoc[$lastArr], $fileUrl);
                                
                                $chat_file_bdy = "files/emails/".$rand."-".$explDoc[$lastArr];
                            }

                            if($this->input->get('photoname') != '') {
                                $explDoc = explode('/', $this->input->get('photoname'));
                                $lastArr = count($explDoc) - 1;

                                $fileUrl = file_get_contents($this->input->get('photoname'));
                                file_put_contents("files/emails/".$rand."-".$explDoc[$lastArr], $fileUrl);

                                $chat_file_bdy = "files/emails/".$rand."-".$explDoc[$lastArr];
                            }

                            $saveChats = $this->Tickets_Model->insert_chat($id_chat_bdy, $id_ticket_bdy, $id_project_bdy, $chat_user_role_bdy, $chat_id_user_bdy, $chat_platform_bdy, $chat_field_bdy, $chat_file_bdy);

                            if($saveChats) {
                                $reply_message .= 'Thanks, Your ticket number is '.$id_ticket;
                            } else {
                                $reply_message .= 'Sorry, Message Send Failed';
                            }
                        } else {
                            $reply_message .= 'Sorry, Message Save Failed';
                        }
                    }
                }
            } else {
                if($this->input->get('text') == '/start') {
                    $reply_message .= 'Hi, for create ticket, type PROJECT_ID <space> Describer what you want for support help';
                } else {
                    $cekTicketSession = $this->Tickets_Model->cek_ticket_session('telegram', $this->input->get('from_id'));
                    if(count($cekTicketSession) > 0) {
                        foreach ($cekTicketSession as $k => $v) {
                            if($v->ticket_status == 'close') {
                                $reply_message .= 'Your ticket have close at '.$v->close_datetime.', for create ticket, type PROJECT_ID <space> Describer what you want for support help';
                            } else {
                                if($this->input->get('from_id') == $v->ticket_from_id) {
                                    $explEmailAddr = explode(';', $this->input->get('from_id'));

                                    $emailFrom = $explEmailAddr[0];
                                    $emailCc = $explEmailAddr[1];

                                    $explResExplEmailAddr = explode(',', $explEmailAddr[0]);

                                    $from_email = $explResExplEmailAddr[0];

                                    if($from_email == $v->ticket_from_id) {

                                        $checkTicketLink = $this->Tickets_Model->cek_ticket_like($explText[0]);
                                        if(count($checkTicketLink) == 0) {
                                            // CHAT SUBJECT
                                            $id_chat                 = $this->Generate_Model->generate_id_user();
                                            $id_ticket_              = $v->id_ticket;
                                            $id_project_             = $value->id_project;
                                            $chat_user_role_         = 'client';
                                            $chat_id_user_           = $from_email;
                                            $chat_platform_          = 'email';
                                            $chat_field_             = $explText[0];
                                            $chat_file               = '';

                                            $this->Tickets_Model->insert_chat($id_chat, $id_ticket_, $id_project_, $chat_user_role_, $chat_id_user_, $chat_platform_, $chat_field_, $chat_file);

                                            // $reply_message .= "470";
                                        }
                                        
                                        // CHAT BODY
                                        $id_chat_bdy                = $this->Generate_Model->generate_id_user();
                                        $id_ticket_bdy              = $v->id_ticket;
                                        $id_project_bdy             = $value->id_project;
                                        $chat_user_role_bdy         = 'client';
                                        $chat_id_user_bdy           = $from_email;
                                        $chat_platform_bdy          = 'email';
                                        $chat_field_bdy             = $explText[1];
                                        $chat_file_bdy              = '';


                                        // FILEEEEE
                                        
                                        $rand = rand($this->input->get('update_id'), date('YmdHis'));

                                        if($this->input->get('document') != '') {
                                            $explDoc = explode('/', $this->input->get('document'));
                                            $lastArr = count($explDoc) - 1;

                                            $fileUrl = file_get_contents($this->input->get('document'));
                                            file_put_contents("files/emails/".$rand."-".$explDoc[$lastArr], $fileUrl);
                                            
                                            $chat_file_bdy = "files/emails/".$rand."-".$explDoc[$lastArr];
                                        }

                                        if($this->input->get('photoname') != '') {
                                            $explDoc = explode('/', $this->input->get('photoname'));
                                            $lastArr = count($explDoc) - 1;

                                            $fileUrl = file_get_contents($this->input->get('photoname'));
                                            file_put_contents("files/emails/".$rand."-".$explDoc[$lastArr], $fileUrl);

                                            $chat_file_bdy = "files/emails/".$rand."-".$explDoc[$lastArr];
                                        }

                                        $saveChats = $this->Tickets_Model->insert_chat($id_chat_bdy, $id_ticket_bdy, $id_project_bdy, $chat_user_role_bdy, $chat_id_user_bdy, $chat_platform_bdy, $chat_field_bdy, $chat_file_bdy);

                                        // $reply_message .= "510";
                                        
                                    } else {
                                        $reply_message .= 'Someone from your team is talking or in the queue to be handled by our support emails';
                                    }
                                } else {
                                    $reply_message .= 'Someone from your team is talking or in the queue to be handled by our support';
                                }
                            }
                        }
                    } else {
                        $reply_message .= 'Your project ID not found';
                    }
                }
            }

            if($reply_message != '') { 

                $reply_message; 

                $explEmailAddr = explode(';', $this->input->get('from_id'));

                $emailFrom = $explEmailAddr[0];
                $emailCc = $explEmailAddr[1];

                $role       = 'ticket';
                $to         = explode(',', $emailFrom);
                $cc         = explode(',', $emailCc);
                $subject    = $explText[0];
                $message    = $reply_message;
                
                $this->Email_Model->send_email($role, $to, $cc, $subject, $message);

                // CEK TIKET
                $cekTiketProject = $this->Tickets_Model->ticket_by_project($projectIdMail);

                $id_log             = $this->Generate_Model->generate_id_user();
                $platform           = 'email';
                $id_ticket          = $cekTiketProject[0]->id_ticket;
                $log_from           = 'tiket@mobiwin.co.id';
                $log_to             = $emailFrom;
                $log_desc           = $reply_message;
                $log_desc_option    = $this->input->get('text');
                $log_response       = 'send';

                $this->Log_Model->log_save($id_log, $platform, $id_ticket, $log_from, $log_to, $log_desc, $log_desc_option, $log_response);

            }
        }
    }
}