<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	 function __construct() {
        parent::__construct();

        // LIBRARY
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('form_validation');

        // HELPER
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');

        // MODEL
        $this->load->model('Users_Model');
        $this->load->model('Projects_Model');
        $this->load->model('Generate_Model');
        $this->load->model('Email_Model');
    }

    public function createproject() {
        if(!$this->session->userdata('admin_session')) {
            redirect(base_url().'signin');
        }

        if($this->session->userdata('handled_session')) {
            redirect(base_url().'chatticket');
        }
        
        $usersList = $this->Users_Model->user_by_status(1);
        $projectList = $this->Projects_Model->project_list();

        $sendData = array(
            'page'          => 'createproject',
            'page_title'    => 'Page Create Project',
            'dash'          => true,


            'users'         => $usersList,
            'projects'      => $projectList 
        );

        $sendData['session_admin_name'] = $this->session->userdata('admin_session_name');
        $sendData['session_admin_email'] = $this->session->userdata('admin_session_email');

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            if(isset($_POST['CreateProject'])) {

                //CreateProject Form
                $this->form_validation->set_rules('ProjectName','Project Name','required');
                $this->form_validation->set_rules('DescriptionProject','Description Project','required');
         
                if($this->form_validation->run() != false){

                    $id_project         = $this->Generate_Model->generate_id_user();
                    $project_name       = $this->input->post('ProjectName');
                    $project_desc       = $this->input->post('DescriptionProject');

                    $save = $this->Projects_Model->project_save($id_project, $project_name, $project_desc);
                    if($save) {
                        $sendData['msg'] = 'New Project Data Inserted';
                    } else {
                        $sendData['msg'] = 'Failed Insert Project Data';
                    }
                    
                    $this->load->view('pages/back/templates/back_template', $sendData);
                } else {
                    
                    $sendData['msg'] = validation_errors();
                    $this->load->view('pages/back/templates/back_template', $sendData);
                }
            } else {

                //BindingProject Form

                $this->form_validation->set_rules('SelectEmploye','Select Employe','required');
                $this->form_validation->set_rules('SelectProject','Select Project','required');
                $this->form_validation->set_rules('Notes','Notes','required');
         
                if($this->form_validation->run() != false){

                    $id_bind            = $this->Generate_Model->generate_id_user();
                    $id_project         = $this->input->post('SelectProject');
                    $id_employe         = $this->input->post('SelectEmploye');
                    $bind_note          = $this->input->post('Notes');

                    $save = $this->Projects_Model->project_bind($id_bind, $id_project, $id_employe, $bind_note);
                    if($save) {
                        $sendData['msg'] = 'New Project Binding Data Inserted';
                    } else {
                        $sendData['msg'] = 'Failed Insert Project Binding Data';
                    }
                    
                    $this->load->view('pages/back/templates/back_template', $sendData);
                } else {
                    
                    $sendData['msg'] = validation_errors();
                    $this->load->view('pages/back/templates/back_template', $sendData);
                }
            }
            
        } else {
            $this->load->view('pages/back/templates/back_template', $sendData);
        }
    }

    public function listproject() {
        if(!$this->session->userdata('admin_session')) {
            redirect(base_url().'signin');
        }

        if($this->session->userdata('handled_session')) {
            redirect(base_url().'chatticket');
        }
        
        $listProject = $this->Projects_Model->project_list();
        $sendData = array(
            'page'          => 'listproject',
            'page_title'    => 'Page List Project',
            'dash'          => true,

            'projects' => $listProject
        );

        $sendData['session_admin_name'] = $this->session->userdata('admin_session_name');
        $sendData['session_admin_email'] = $this->session->userdata('admin_session_email');

        $this->load->view('pages/back/templates/back_template', $sendData);
    }
}