<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends CI_Controller {

	 function __construct() {
        parent::__construct();

        // LIBRARY
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('form_validation');

        // HELPER
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');

        // MODEL
        $this->load->model('Users_Model');
        $this->load->model('Projects_Model');
        $this->load->model('Generate_Model');
        $this->load->model('Email_Model');
        $this->load->model('Tickets_Model');
        $this->load->model('Telegram_Model');
        $this->load->model('Log_Model');
    }

    public function listticket() {
        if(!$this->session->userdata('admin_session')) {
            redirect(base_url().'signin');
        }
        
        if(isset($_GET['ticket'])) {
            if(isset($_GET['handle'])) {
                $handle_by = $this->session->userdata('admin_session');
                $handled = $this->Tickets_Model->update_handle_ticket($handle_by, $this->input->get('ticket'));
                if($handled) {
                    $this->session->set_userdata('handled_session', $this->input->get('ticket'));

                    $getTicketByID = $this->Tickets_Model->ticket_by_id($this->input->get('ticket'));
                    if(count($getTicketByID) > 0) {
                        foreach ($getTicketByID as $key => $value) {
                            $userAdmin = $this->Users_Model->user_by_idemploye($value->handle_by);
                             if(count($userAdmin) > 0) {
                                foreach ($userAdmin as $k => $v) {

                                    $message_field  = "Your ticket handle with ".ucfirst($v->name_employe);

                                    if(strtolower($value->ticket_from_platform) == 'telegram') {
                                        $from_id        = $value->ticket_from_id;

                                        $sendPlatform = $this->Telegram_Model->send_message($from_id, $message_field);
                                    } elseif(strtolower($value->ticket_from_platform) == 'email') {

                                        $role       = 'ticket';
                                        $listTo     = $value->ticket_from_id;
                                        $listCc     = '';
                                        $subject    = $value->ticket_desc;

                                        // LAST CHAT
                                        $lastChatIdTicket = $this->Tickets_Model->last_ticket_chat_idticket($this->input->get('ticket'));
                                        if(count($lastChatIdTicket) > 0) {
                                            $lastChatFinal = end($lastChatIdTicket);

                                            $message = $message_field.'<br><br><br>'.$lastChatFinal->chat_field;
                                        } else {
                                            $message = $message_field;
                                        }

                                        
                                        $sendPlatform = $this->Email_Model->send_email($role, $listTo, $listCc, $subject, $message);
                                    }

                                }
                            }
                        }
                    }

                    redirect(base_url().'chatticket');
                } else {
                    redirect(base_url().'listticket');
                }
            } else {
                $ticketId = $this->Tickets_Model->ticket_by_id($this->input->get('ticket'));
                $this->output->set_content_type('application/json')
                ->set_output(json_encode($ticketId));
            }
        } else {
            if($this->session->userdata('handled_session')) {
                redirect(base_url().'chatticket');
            }

            $listTicket = $this->Tickets_Model->ticket_by_status('open');

            $sendData = array(
                'page'          => 'listticket',
                'page_title'    => 'Page List Ticket',
                'dash'          => true,

                'tickets' => $listTicket
            );

            $sendData['session_admin_name'] = $this->session->userdata('admin_session_name');
            $sendData['session_admin_email'] = $this->session->userdata('admin_session_email');


            $this->load->view('pages/back/templates/back_template', $sendData);
        }
    }

    public function chatticket() {
        if(!$this->session->userdata('admin_session')) {
            redirect(base_url().'signin');
        }

        if(isset($_GET['adm'])) {

            if(isset($_GET['text'])) {
                if($this->input->get('text') != '') {

                    $ticketAdmin = $this->Tickets_Model->ticket_by_admin($this->input->get('adm'));
                    if(count($ticketAdmin) > 0) {
                        foreach ($ticketAdmin as $key => $value) {

                            $id_chat        = $this->Generate_Model->generate_id_user();
                            $id_ticket      = $value->id_ticket;
                            $id_project     = $value->id_project;
                            $chat_user_role = 'admin';
                            $chat_id_user   = $this->input->get('adm');
                            $chat_platform  = $value->ticket_from_platform;
                            $chat_field     = $this->input->get('text');
                            $chat_file      = '';

                            // Platform id ticket_from_id

                            $saveChat = $this->Tickets_Model->insert_chat($id_chat, $id_ticket, $id_project, $chat_user_role, $chat_id_user, $chat_platform, $chat_field,  $chat_file);

                            if($saveChat) {
                                $from_id        = $value->ticket_from_id;
                                $message_field  = $this->input->get('text');


                                if(strtolower($chat_platform) == 'telegram') {
                                    $sendPlatform = $this->Telegram_Model->send_message($from_id, $message_field);
                                } elseif(strtolower($chat_platform) == 'email') {

                                    $role       = 'ticket';
                                    $listTo     = $from_id;
                                    $listCc     = '';
                                    $subject    = $value->ticket_desc;

                                    // LAST CHAT
                                    $lastChatIdTicket = $this->Tickets_Model->last_ticket_chat_idticket($id_ticket);
                                    if(count($lastChatIdTicket) > 0) {
                                        $lastChatFinal = end($lastChatIdTicket);

                                        $message = $chat_field.'<br><br><br>'.$lastChatFinal->chat_field;
                                    } else {
                                        $message = $chat_field;
                                    }

                                    
                                    $sendPlatform = $this->Email_Model->send_email($role, $listTo, $listCc, $subject, $message);
                                }

                                // SEND MESSAGE
                                

                                // SAVE LOG
                                $id_log             = $this->Generate_Model->generate_id_user();
                                $platform           = $chat_platform;
                                $id_ticket_         = $id_ticket;
                                $log_from           = $chat_id_user;
                                $log_to             = $from_id;
                                $log_desc           = $this->input->get('text');
                                $log_desc_option    = $sendPlatform['option'];
                                $log_response       = $sendPlatform['results'];

                                $saveLog = $this->Log_Model->log_save($id_log, $platform, $id_ticket_, $log_from, $log_to, $log_desc, $log_desc_option, $log_response);

                                if($saveLog) {
                                    $result = array(
                                        'stat' => 1,
                                        'msg' => 'Message send'
                                    );
                                } else {
                                    $result = array(
                                        'stat' => 0,
                                        'msg' => 'Failed save log'
                                    );
                                }
                            } else {
                                $result = array(
                                    'stat' => 0,
                                    'msg' => 'Failed save chat'
                                );
                            }
                        }
                    } else {
                        $result = array(
                            'stat' => 0,
                            'msg' => 'Ticket by admin not found'
                        );
                    }                    
                } else {
                    $result = array(
                        'stat' => 0,
                        'msg' => 'Reply Empty'
                    );
                }

                $this->output->set_content_type('application/json')
                ->set_output(json_encode($result));
            } else {

                if(isset($_GET['unread'])) {

                    $chatListHandled = $this->Tickets_Model->chat_list_unread($this->input->get('adm'));

                    $newChatData = array();

                    foreach ($chatListHandled as $key => $value) {
                        $newChatData[$key]['id_chat']           = $value->id_chat;
                        $newChatData[$key]['id_ticket']         = $value->id_ticket;
                        $newChatData[$key]['project_name']      = $value->project_name;
                        $newChatData[$key]['chat_user_role']    = $value->chat_user_role;

                        if($value->chat_user_role == 'admin') {
                            $userIs = ucfirst($value->name_employe);
                        } elseif($value->chat_user_role == 'client') {
                            $userIs = 'Client : '.$value->chat_user_id;
                        } else {
                            $userIs = $value->chat_user_id;
                        }

                        $newChatData[$key]['chat_user_id']      = $userIs;
                        $newChatData[$key]['chat_platform']     = $value->chat_platform;
                        $newChatData[$key]['chat_field']        = $value->chat_field;


                        // Make READ
                        $this->Tickets_Model->update_chat_read('1', $value->id_chat);
                    }

                    $this->output->set_content_type('application/json')
                    ->set_output(json_encode($newChatData));

                } else {

                    if(isset($_GET['close'])) {
                        ////////////// CLOSE TICKET
                        $ticketAdmin = $this->Tickets_Model->ticket_by_admin($this->input->get('adm'));
                        if(count($ticketAdmin) > 0) {
                            foreach ($ticketAdmin as $key => $value) {
                                // CHANGE TICKET STATUS
                                $closeTicket = $this->Tickets_Model->update_close_ticket($value->id_ticket);
                                if($closeTicket) {

                                    // SEND CLOSING MESSAGE
                                    $getTicketByID = $this->Tickets_Model->ticket_by_id($value->id_ticket);
                                    if(count($getTicketByID) > 0) {
                                        foreach ($getTicketByID as $ke => $va) {
                                            $userAdmin = $this->Users_Model->user_by_idemploye($va->handle_by);
                                             if(count($userAdmin) > 0) {
                                                foreach ($userAdmin as $k => $v) {

                                                    $message_field  = "Thanks, Your ticket have close with ".ucfirst($v->name_employe);

                                                    if(strtolower($va->ticket_from_platform) == 'telegram') {
                                                        $from_id        = $va->ticket_from_id;

                                                        $sendPlatform = $this->Telegram_Model->send_message($from_id, $message_field);
                                                    } elseif(strtolower($va->ticket_from_platform) == 'email') {

                                                        $role       = 'ticket';
                                                        $listTo     = $va->ticket_from_id;
                                                        $listCc     = '';
                                                        $subject    = $va->ticket_desc;

                                                        // LAST CHAT
                                                        $lastChatIdTicket = $this->Tickets_Model->last_ticket_chat_idticket($value->id_ticket);
                                                        if(count($lastChatIdTicket) > 0) {
                                                            $lastChatFinal = end($lastChatIdTicket);

                                                            $message = $message_field.'<br><br><br>'.$lastChatFinal->chat_field;
                                                        } else {
                                                            $message = $message_field;
                                                        }

                                                        
                                                        $sendPlatform = $this->Email_Model->send_email($role, $listTo, $listCc, $subject, $message);
                                                    }

                                                }
                                            }
                                        }
                                    }


                                    // DESTROY SESSION HANDLE TICKET
                                    $this->session->unset_userdata('handled_session');

                                    // REDIRECT TO LIST TICKET
                                    redirect(base_url().'listticket');
                                } else {
                                    redirect(base_url().'chatticket?closeTicket=false');
                                }
                            }
                        } else {
                            // REDIRECT TO LIST TICKET - FALSE
                            redirect(base_url().'listticket?ticketAdmin=false');
                        }
                    } else {
                        $chatListHandled = $this->Tickets_Model->chat_list($this->input->get('adm'));

                        $newChatData = array();

                        foreach ($chatListHandled as $key => $value) {
                            $newChatData[$key]['id_chat']           = $value->id_chat;
                            $newChatData[$key]['id_ticket']         = $value->id_ticket;
                            $newChatData[$key]['project_name']      = $value->project_name;
                            $newChatData[$key]['chat_user_role']    = $value->chat_user_role;

                            if($value->chat_user_role == 'admin') {
                                $userIs = ucfirst($value->name_employe);
                            } elseif($value->chat_user_role == 'client') {
                                $userIs = 'Client : '.$value->chat_user_id;
                            } else {
                                $userIs = $value->chat_user_id;
                            }

                            $newChatData[$key]['chat_user_id']      = $userIs;
                            $newChatData[$key]['chat_platform']     = $value->chat_platform;
                            $newChatData[$key]['chat_field']        = $value->chat_field;
                            $newChatData[$key]['chat_file']         = base_url().$value->chat_file;

                            $explFilename = explode('/', $value->chat_file);
                            $lastFileExpl = count($explFilename) - 1;


                            $newChatData[$key]['chat_filename']     = $explFilename[$lastFileExpl];
                        }


                        $this->output->set_content_type('application/json')
                        ->set_output(json_encode($newChatData));
                    }
                }
            }
        } else {

            $id_ticket = $this->session->userdata('handled_session');
            $cekTicket = $this->Tickets_Model->ticket_by_id($id_ticket);
            if(count($cekTicket) > 0) {
                $sendData = array(
                    'page'          => 'chatticket',
                    'page_title'    => 'Page Chat Ticket',
                    'dash'          => true,

                    'socket'        => true
                );

                $sendData['session_admin'] = $this->session->userdata('admin_session');
                $sendData['session_admin_name'] = $this->session->userdata('admin_session_name');
                $sendData['session_admin_email'] = $this->session->userdata('admin_session_email');

                $this->load->view('pages/back/templates/back_template', $sendData);
            } else {
                // DESTROY SESSION HANDLE TICKET
                $this->session->unset_userdata('handled_session');

                // REDIRECT TO LIST TICKET
                redirect(base_url().'listticket');
            }
        }
    }
}