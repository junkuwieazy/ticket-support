<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	 function __construct() {
        parent::__construct();

        // LIBRARY
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('form_validation');

        // HELPER
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');

        // MODEL
        $this->load->model('Users_Model');
        $this->load->model('Generate_Model');
        $this->load->model('Email_Model');
        $this->load->model('Tickets_Model');
        $this->load->model('Projects_Model');
    }

    

    public function index() {   
        // $this->Email_Model->get_email(); exit();
        if($this->session->userdata('admin_session')) {
            redirect(base_url().'dashboard');
        }     


        $sendData = array(
            'page'          => 'login',
            'page_title'    => 'Page Login',
            'dash'          => false,
        );

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('WorkEmailAddress','Work Email Address','required|trim|valid_email');
            $this->form_validation->set_rules('EmployePassword','Employe Password','required');
     
            if($this->form_validation->run() != false){

                $checkLogin = $this->Users_Model->user_login($this->input->post('WorkEmailAddress'), md5($this->input->post('EmployePassword')));
                if(count($checkLogin) > 0) {
                    foreach ($checkLogin as $key => $value) {
                        // SET SESSION LOGIN
                        $this->session->set_userdata('admin_session', $value->id_employe);
                        $this->session->set_userdata('admin_session_name', $value->name_employe);
                        $this->session->set_userdata('admin_session_email', $value->email_employe);
                        $this->session->set_userdata('admin_session_status', $value->employe_status);
                        // REDIREC TO DASHBOARD
                        redirect(base_url().'dashboard'); 
                    }
                } else {
                    $sendData['msg'] = 'Email and Password not match';
                }
            } else {
                
                $sendData['msg'] = validation_errors();
            }

            $this->load->view('pages/back/templates/back_template', $sendData);
        }


        $this->load->view('pages/back/templates/back_template', $sendData);
    }

    public function dashboard() {
        if(!$this->session->userdata('admin_session')) {
            redirect(base_url().'signin');
        }

        if($this->session->userdata('handled_session')) {
            redirect(base_url().'chatticket');
        }

        $sendData = array(
            'page'          => 'dashboard',
            'page_title'    => 'Page Dashboard',
            'dash'          => true, 
        );

        $sendData['logfour'] = $this->Tickets_Model->ticket_limit();

        $forSum = '';
        $projct_count = $this->Projects_Model->project_chart();
        foreach ($projct_count as $key => $value) {
            $forSum += $value->jproj;
        }

        $percenChart = 100 / $forSum;

        $pData = '';
        $pName = '';

        $hexaRand1 = '';
        $hexaRand2 = '';

        foreach ($projct_count as $key => $value) {
            $pData .= $value->jproj * $percenChart.',';
            $pName .= "'".$value->project_name."'".',';

            $hexaRand1 .= "'".'#' .str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT)."'".',';
            $hexaRand2 .= "'".'#' .str_pad(dechex(rand(0xFFFFFF, 0x000000)), 6, 0, STR_PAD_LEFT)."'".',';
        }

        
        $sendData['percen_block'] = array(
            'data'                      => $pData,
            'backgroundColor'           => $hexaRand1,
            'hoverBackgroundColor'      => $hexaRand2,
            'labels'                    => $pName,
        );

        $sendData['project_list'] = $this->Tickets_Model->ticket_limit(); 
        $sendData['tickets_list'] = $this->Tickets_Model->ticket_list(); 

        $sendData['session_admin_name'] = $this->session->userdata('admin_session_name');
        $sendData['session_admin_email'] = $this->session->userdata('admin_session_email');

        $this->load->view('pages/back/templates/back_template', $sendData);
    }

    public function createproject() {
        if(!$this->session->userdata('admin_session')) {
            redirect(base_url().'signin');
        }

        if($this->session->userdata('handled_session')) {
            redirect(base_url().'chatticket');
        }

        $sendData = array(
            'page'          => 'createproject',
            'page_title'    => 'Page Create Project',
            'dash'          => true,
        );

        $sendData['session_admin_name'] = $this->session->userdata('admin_session_name');
        $sendData['session_admin_email'] = $this->session->userdata('admin_session_email');

        $this->load->view('pages/back/templates/back_template', $sendData);
    }

    public function createuser() {
        if(!$this->session->userdata('admin_session')) {
            redirect(base_url().'signin');
        }

        if($this->session->userdata('handled_session')) {
            redirect(base_url().'chatticket');
        }
        
        $sendData = array(
            'page'          => 'createuser',
            'page_title'    => 'Page Create User',
            'dash'          => true,
        );

        $sendData['session_admin_name'] = $this->session->userdata('admin_session_name');
        $sendData['session_admin_email'] = $this->session->userdata('admin_session_email');

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            
            $this->form_validation->set_rules('EmployeName','Employe Name','required');
            $this->form_validation->set_rules('EmployeEmail','Employe Email','required|trim|valid_email');
     
            if($this->form_validation->run() != false){

                $checkEmail = $this->Users_Model->user_by_email($this->input->post('EmployeEmail'));
                if(count($checkEmail) > 0) {
                    $sendData['msg'] = $this->input->post('EmployeEmail').' already registered';
                } else {
                    $plain_password     = $this->Generate_Model->generate_id_user();

                    $role       = 'ticket';
                    $listTo     = $this->input->post('EmployeEmail');
                    $listCc     = '';
                    $subject    = 'New Employe Submited';
                    $message    = 'Selamat bergabung dalam Mobiwin, berikut password anda : <b>'.$plain_password.'</b>.<br> Silahkan <a href='.base_url().'signin>Sign In</a>. Terima kasih';


                    $sendEmail = $this->Email_Model->send_email($role, $listTo, $listCc, $subject, $message);

                    if($sendEmail['results'] == 'Email Failed') {
                        $sendData['msg'] = 'Failed Send Email to Employe';
                    } else {
                        $id_employe         = $this->Generate_Model->generate_id_user();
                        $name_employe       = $this->input->post('EmployeName');
                        $email_employe      = $this->input->post('EmployeEmail');
                        $password_employe   = md5($plain_password);

                        $save = $this->Users_Model->user_save($id_employe, $name_employe, $email_employe, $password_employe);
                        if($save) {
                            $sendData['msg'] = 'New Employe Data Inserted';
                        } else {
                            $sendData['msg'] = 'Failed Insert Employe Data';
                        }
                    }
                }
            } else {
                
                $sendData['msg'] = validation_errors();
            }

            $this->load->view('pages/back/templates/back_template', $sendData);
        }


        $this->load->view('pages/back/templates/back_template', $sendData);
    }

    public function updatepassword()
    {
        if(!$this->session->userdata('admin_session')) {
            redirect(base_url().'signin');
        }

        if($this->session->userdata('handled_session')) {
            redirect(base_url().'chatticket');
        }
        
        $sendData = array(
            'page'          => 'updatepassword',
            'page_title'    => 'Page Update Password',
            'dash'          => true,
        );

        $sendData['session_admin_name'] = $this->session->userdata('admin_session_name');
        $sendData['session_admin_email'] = $this->session->userdata('admin_session_email');

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            
            $this->form_validation->set_rules('OldPassword','Old Password','required');
            $this->form_validation->set_rules('NewPassword','New Password','required');
            $this->form_validation->set_rules('ReNewPassword','Re-New Password','required');
     
            if($this->form_validation->run() != false){

                // admin_session_email

                $checkUser = $this->Users_Model->user_login($this->session->userdata('admin_session_email'), md5($this->input->post('OldPassword')));
                if(count($checkUser) > 0) {
                    if($this->input->post('NewPassword') == $this->input->post('ReNewPassword')) {
                        $updateUserPassword = $this->Users_Model->update_user_password(md5($this->input->post('NewPassword')),$this->session->userdata('admin_session'));

                        if($updateUserPassword) {
                            // LOGIN ADMIN
                            $this->session->unset_userdata('admin_session');
                            redirect(base_url().'signin');
                        } else {
                            $sendData['msg'] = 'Failed update passoword';
                        }
                    } else {
                        $sendData['msg'] = 'Passwordt not match';
                    }
                } else {
                    $sendData['msg'] = 'Your old password is wrong';
                }
            } else {                
                $sendData['msg'] = validation_errors();
            }

            $this->load->view('pages/back/templates/back_template', $sendData);
        }


        $this->load->view('pages/back/templates/back_template', $sendData);
    }

    public function listuser() {
        if(!$this->session->userdata('admin_session')) {
            redirect(base_url().'signin');
        }

        if($this->session->userdata('handled_session')) {
            redirect(base_url().'chatticket');
        }
        
        $listEmploye = $this->Users_Model->user_by_status(1);

        $sendData = array(
            'page'          => 'listuser',
            'page_title'    => 'Page List User',
            'dash'          => true,

            'employes' => $listEmploye
        );

        $sendData['session_admin_name'] = $this->session->userdata('admin_session_name');
        $sendData['session_admin_email'] = $this->session->userdata('admin_session_email');
        
        $this->load->view('pages/back/templates/back_template', $sendData);
    }

    public function signout() {
        // LOGIN ADMIN
        $this->session->unset_userdata('admin_session');
        redirect(base_url().'signin');
    }
}