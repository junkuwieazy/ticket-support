<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {

	 function __construct() {
        parent::__construct();

        // LIBRARY
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('form_validation');

        // HELPER
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');

        // MODEL
        // $this->load->model('Game_Model');
    }

    public function index() {
        redirect('http://muloska.com');
    }
}