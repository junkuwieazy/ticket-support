<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_Model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();

        $this->load->library('email');
    }

    public function send_email($role, $listTo, $listCc, $subject, $message) {

        if($role == 'cs') {
            $from = 'cs@mobiwin.co.id';
            $name = 'Customer Service';
        } elseif($role == 'sms') {
            $from = 'sms@mobiwin.co.id';
            $name = 'IT SMS';
        } elseif($role == 'info') {
            $from = 'info@mobiwin.co.id';
            $name = 'Info Mobiwin';
        } elseif($role == 'ticket') {
            $from = 'tiket@mobiwin.co.id';
            $name = 'Ticket Support';
        }

        $ci = get_instance();
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://mail.mobiwin.co.id";
        $config['smtp_port'] = 465;
        $config['smtp_user'] = "tiket@mobiwin.co.id";
        $config['smtp_pass'] = "tiket2019";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $ci->email->initialize($config);

        $ci->email->from($from, $name);

        $ci->email->to($listTo);

        if($listCc != '') {
            $ci->email->cc($listCc);
        }
        
        $ci->email->subject($subject);
        $ci->email->message($message);


        if ($this->email->send()) {
            return array(
                'results' => 'Email Sended',
                'option' => 'Email Sended'
            );
        } else {
            return array(
                'results' => 'Email Failed',
                'option' => 'Email Failed : '.$this->email->print_debugger()
            );
        }
    }
}