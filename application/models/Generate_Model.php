<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generate_Model extends CI_Model {

	 function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function generate_id_user() {

    	$id_employe_gen = rand(12,99).rand(11,99).rand(10,99);

    	$sql = "SELECT id_employe FROM employe_tb WHERE id_employe = " . $this->db->escape($id_employe_gen);
        $query = $this->db->query($sql);
		$numRows = $query->num_rows();

		if($numRows == 0) {
			$id_gen = $id_employe_gen;
		}

        return $id_gen;
    }
}