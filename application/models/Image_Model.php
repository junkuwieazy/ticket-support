<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_Model extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->load->database();
}

public function get_image_from_url($image_name, $platform, $url) {
    if (!file_exists($url)) {
         $res = false;
    } else {
        $extFile = strtolower(pathinfo($url, PATHINFO_EXTENSION));
        $imageContent = file_get_contents($url);
        if(file_put_contents('/files/apis/'.$platform.'/'.$image_name.'.'.$extFile, $imageContent)) {
            $res = true;
        } else {
            $res = false;
        }
    }

}