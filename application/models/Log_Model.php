<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_Model extends CI_Model {

     function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function log_save($id_log, $platform, $id_ticket, $log_from, $log_to, $log_desc, $log_desc_option, $log_response) {  
        $this->id_log           = $id_log; 
        $this->platform         = $platform;
        $this->id_ticket        = $id_ticket;
        $this->log_from         = $log_from;
        $this->log_to           = $log_to;
        $this->log_desc         = $log_desc;
        $this->log_desc_option  = $log_desc_option;
        $this->log_response     = $log_response;
        $this->log_datetime     = date('Y-m-d H:i:s');

        $this->db->insert('platform_log_tb', $this);
        return $this->db->affected_rows();
    }

    public function user_by_status($employe_status) {
        $sql = "SELECT * FROM employe_tb WHERE employe_status = " . $this->db->escape($employe_status);
        $query = $this->db->query($sql);
        return $query->result();
    }
}