<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects_Model extends CI_Model {

	 function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function project_by_idproject($id_project) {
    	$sql = "SELECT * FROM project_tb WHERE id_project = " . $this->db->escape($id_project);
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function project_save($id_project, $project_name, $project_desc) {	
    	$this->id_project    	= $id_project; 
        $this->project_name  	= $project_name;
        $this->project_desc     = $project_desc;
        $this->project_datetime = date('Y-m-d H:i:s');

        $this->db->insert('project_tb', $this);
        return $this->db->affected_rows();
    }

    public function project_bind($id_bind, $id_project, $id_employe, $bind_note) {   
        $this->id_bind          = $id_bind; 
        $this->id_project       = $id_project;
        $this->id_employe       = $id_employe;
        $this->bind_note        = $bind_note;
        $this->bind_datetime    = date('Y-m-d H:i:s');

        $this->db->insert('project_bind_tb', $this);
        return $this->db->affected_rows();
    }

    public function project_list() {
        $sql = "SELECT * FROM project_tb";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function project_chart() {
        // $sql = "SELECT * FROM ticket_tb WHERE ticket_status = " . $this->db->escape($ticket_status);
        $sql = "SELECT COUNT(pj.project_inc) jproj,pj.project_name 
        FROM project_tb pj 
        INNER JOIN ticket_tb tc ON tc.id_project = pj.id_project GROUP BY pj.id_project";
        $query = $this->db->query($sql);
        return $query->result();
    }
}