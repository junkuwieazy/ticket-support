<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Telegram_Model extends CI_Model {

     function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function send_message($from_id, $message_field) {
        $sendMessageURL = "https://api.telegram.org/bot857032687:AAHAMmW_EydGDsTEFIv46pA8ymBixAFN8WQ/sendMessage?text=".urlencode($message_field)."&chat_id=".$from_id;

        $curlSendMessage = curl_init();
        curl_setopt_array($curlSendMessage, array(
            CURLOPT_URL => $sendMessageURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: 7116f29b-4d05-4497-9c03-b9fad7afe299",
                "cache-control: no-cache"
            ),
        ));

        $res = curl_exec($curlSendMessage);
        curl_close($curlSendMessage);

        return array(
            'results'   => $res,
            'option'    => $sendMessageURL
        );
    }

    public function user_save($id_employe, $name_employe, $email_employe, $password_employe) {  
        $this->id_employe       = $id_employe; 
        $this->name_employe     = $name_employe;
        $this->email_employe    = $email_employe;
        $this->password_employe = $password_employe;
        $this->employe_status   = 1;
        $this->employe_datetime = date('Y-m-d H:i:s');

        $this->db->insert('employe_tb', $this);
        return $this->db->affected_rows();
    }

    public function user_by_status($employe_status) {
        $sql = "SELECT * FROM employe_tb WHERE employe_status = " . $this->db->escape($employe_status);
        $query = $this->db->query($sql);
        return $query->result();
    }
}