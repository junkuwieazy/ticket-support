<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets_Model extends CI_Model {

	 function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function ticket_by_id($id_ticket) {
        // $sql = "SELECT * FROM ticket_tb WHERE ticket_status = " . $this->db->escape($ticket_status);
        $sql = "SELECT tc.id_ticket, pj.project_name, tc.ticket_priority, tc.ticket_from_platform, tc.ticket_desc, tc.handle_by, tc.ticket_from_id FROM ticket_tb tc INNER JOIN project_tb pj ON tc.id_project = pj.id_project WHERE id_ticket = " . $this->db->escape($id_ticket) . " LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function ticket_limit() {
        // $sql = "SELECT * FROM ticket_tb WHERE ticket_status = " . $this->db->escape($ticket_status);
        $sql = "SELECT tc.id_ticket, pj.project_name, tc.ticket_priority, tc.ticket_from_platform, tc.ticket_desc, tc.handle_by, em.name_employe, tc.ticket_from_id, tc.handle_datetime, tc.close_datetime, tc.ticket_status
            FROM ticket_tb tc 
            INNER JOIN project_tb pj ON tc.id_project = pj.id_project 
            INNER JOIN employe_tb em ON tc.handle_by = em.id_employe
            LIMIT 4";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function ticket_list() {
        $sql = "SELECT tc.id_ticket, pj.project_name, tc.ticket_priority, tc.ticket_from_platform, tc.ticket_desc, tc.handle_by, em.name_employe, tc.ticket_from_id, tc.handle_datetime, tc.close_datetime, tc.ticket_status, tc.ticket_datetime
            FROM ticket_tb tc 
            INNER JOIN project_tb pj ON tc.id_project = pj.id_project 
            INNER JOIN employe_tb em ON tc.handle_by = em.id_employe";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function ticket_by_project($id_project) {
        $sql = "SELECT * FROM ticket_tb WHERE id_project = " . $this->db->escape($id_project);
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function ticket_by_status($ticket_status) {
        // $sql = "SELECT * FROM ticket_tb WHERE ticket_status = " . $this->db->escape($ticket_status);
        $sql = "SELECT tc.id_ticket, pj.project_name, tc.ticket_priority, tc.ticket_from_platform, tc.ticket_desc FROM ticket_tb tc INNER JOIN project_tb pj ON tc.id_project = pj.id_project WHERE tc.ticket_status = " . $this->db->escape($ticket_status);
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function ticket_by_admin($id_admin) {
        $sql ="SELECT tc.id_project, tc.id_ticket, tc.ticket_from_platform, tc.ticket_from_id, tc.ticket_desc
            FROM ticket_tb tc
            INNER JOIN project_tb pj ON tc.id_project = pj.id_project
            INNER JOIN employe_tb ep ON tc.handle_by = ep.id_employe
            WHERE tc.ticket_status = 'handled' AND ep.id_employe = " . $this->db->escape($id_admin) . " LIMIT 1";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function cek_ticket_session($ticket_from_platform, $ticket_from_id) {
        $sql = "SELECT * FROM ticket_tb WHERE ticket_from_platform = " . $this->db->escape($ticket_from_platform) . " AND ticket_from_id = " . $this->db->escape($ticket_from_id) . " ORDER BY ticket_inc DESC LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function cek_ticket_like($ticket_desc) {
        $sql = "SELECT * FROM ticket_tb WHERE ticket_desc LIKE '%" . $ticket_desc . "' LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function ticket_save($id_ticket, $id_project, $ticket_desc, $ticket_priority, $ticket_from_platform, $ticket_from_id, $ticket_status) {	
    	$this->id_ticket    	    = $id_ticket; 
        $this->id_project  	        = $id_project;
        $this->ticket_desc          = $ticket_desc;
        $this->ticket_priority      = $ticket_priority;
        $this->ticket_from_platform = $ticket_from_platform;
        $this->ticket_from_id       = $ticket_from_id;
        $this->ticket_status        = $ticket_status;
        $this->ticket_datetime      = date('Y-m-d H:i:s');

        $this->db->insert('ticket_tb', $this);
        return $this->db->affected_rows();
    }

    public function insert_chat($id_chat, $id_ticket, $id_project, $chat_user_role, $chat_user_id, $chat_platform, $chat_field, $chat_file) {

        $chat_read = '2';
        $chat_status = '1';
        $chat_datetime = date('Y-m-d H:i:s');

        $sql = "INSERT INTO chat_tb(id_chat,id_ticket,id_project,chat_user_role,chat_user_id,chat_platform,chat_field,chat_read,chat_file,chat_status,chat_datetime) "
            . "VALUES("
            . "" . $this->db->escape($id_chat) . ", "
            . "" . $this->db->escape($id_ticket) . ", "
            . "" . $this->db->escape($id_project) . ", "
            . "" . $this->db->escape($chat_user_role) . ", "
            . "" . $this->db->escape($chat_user_id) . ", "
            . "" . $this->db->escape($chat_platform) . ", "
            . "" . $this->db->escape($chat_field) . ", "
            . "" . $this->db->escape($chat_read) . ", "
            . "" . $this->db->escape($chat_file) . ", "
            . "" . $this->db->escape($chat_status) . ", "
            . "" . $this->db->escape($chat_datetime) . ")";

        $this->db->query($sql);
        return $this->db->affected_rows();

    }

    public function update_handle_ticket($handle_by, $id_ticket) {
        $set = array(
            'handle_by'         => $handle_by,
            'ticket_status'     => 'handled',
            'handle_datetime'   => date('Y-m-d H:i:s')
        );

        $this->db->set($set);
        $this->db->where('id_ticket', $id_ticket);
        if($this->db->update('ticket_tb')) {
            return true;
        } else {
            return false;
        }
    }

    public function update_close_ticket($id_ticket) {
        $set = array(
            'ticket_status'     => 'close',
            'close_datetime'   => date('Y-m-d H:i:s')
        );

        $this->db->set($set);
        $this->db->where('id_ticket', $id_ticket);
        if($this->db->update('ticket_tb')) {
            return true;
        } else {
            return false;
        }
    }

    public function chat_list($id_admin) {
        $sql ="SELECT ct.id_chat, ct.id_ticket, pj.project_name, ct.chat_user_role, ct.chat_user_id, ep.name_employe, ct.chat_platform, ct.chat_field, ct.chat_file
            FROM chat_tb ct 
            INNER JOIN project_tb pj ON ct.id_project = pj.id_project
            INNER JOIN ticket_tb tc ON tc.id_ticket = ct.id_ticket
            INNER JOIN employe_tb ep ON tc.handle_by = ep.id_employe
            WHERE tc.ticket_status = 'handled' AND ep.id_employe = " . $this->db->escape($id_admin) . "";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function chat_list_unread($id_admin) {
        $sql ="SELECT ct.id_chat, ct.id_ticket, pj.project_name, ct.chat_user_role, ct.chat_user_id, ep.name_employe, ct.chat_platform, ct.chat_field 
            FROM chat_tb ct 
            INNER JOIN project_tb pj ON ct.id_project = pj.id_project
            INNER JOIN ticket_tb tc ON tc.id_ticket = ct.id_ticket
            INNER JOIN employe_tb ep ON tc.handle_by = ep.id_employe
            WHERE tc.ticket_status = 'handled' AND chat_read = '2' AND ep.id_employe = " . $this->db->escape($id_admin) . "";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function update_chat_read($chat_read, $id_chat) {
        $set = array(
            'chat_read'         => $chat_read,
        );

        $this->db->set($set);
        $this->db->where('id_chat', $id_chat);
        if($this->db->update('chat_tb')) {
            return true;
        } else {
            return false;
        }
    }

    public function last_ticket_chat_chatinc($chat_inc) {
        $sql = "SELECT chat_field FROM chat_tb WHERE chat_inc = " . $this->db->escape($chat_inc) . " ORDER BY chat_inc DESC LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function last_ticket_chat_idchat($id_chat) {
        $sql = "SELECT chat_inc FROM chat_tb WHERE id_chat = " . $this->db->escape($id_chat) . " ORDER BY chat_inc DESC LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function last_ticket_chat_idticket($id_ticket) {
        $sql = "SELECT * FROM chat_tb WHERE id_ticket = " . $this->db->escape($id_ticket) . " ORDER BY chat_inc DESC LIMIT 2";
        $query = $this->db->query($sql);
        return $query->result();
    }
}