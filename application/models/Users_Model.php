<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_Model extends CI_Model {

	 function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function user_by_idemploye($id_employe) {
        $sql = "SELECT * FROM employe_tb WHERE id_employe = " . $this->db->escape($id_employe);
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function user_by_email($email_employe) {
    	$sql = "SELECT * FROM employe_tb WHERE email_employe = " . $this->db->escape($email_employe);
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function user_login($email_employe, $password_employe) {
        $sql = "SELECT * FROM employe_tb WHERE email_employe = " . $this->db->escape($email_employe) . " AND password_employe = " . $this->db->escape($password_employe) . " LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function user_save($id_employe, $name_employe, $email_employe, $password_employe) {	
    	$this->id_employe    	= $id_employe; 
        $this->name_employe  	= $name_employe;
        $this->email_employe    = $email_employe;
        $this->password_employe = $password_employe;
        $this->employe_status   = 1;
        $this->employe_datetime = date('Y-m-d H:i:s');

        $this->db->insert('employe_tb', $this);
        return $this->db->affected_rows();
    }

    public function user_by_status($employe_status) {
    	$sql = "SELECT * FROM employe_tb WHERE employe_status = " . $this->db->escape($employe_status);
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function update_user_password($new_password, $id_employe) {
        $set = array(
            'password_employe'  => $new_password,
        );

        $this->db->set($set);
        $this->db->where('id_employe', $id_employe);
        if($this->db->update('employe_tb')) {
            return true;
        } else {
            return false;
        }
    }
}