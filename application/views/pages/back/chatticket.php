<!-- modal static -->
<div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
 data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticModalLabel">Close Ticket Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    You are sure you want to close this ticket, make sure all discussions have been agreed and confirmed for closure to the client.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <a href="<?php echo base_url(); ?>chatticket?adm=<?php echo $session_admin; ?>&close=true" class="btn btn-primary">Confirm</a>
            </div>
        </div>
    </div>
</div>
<!-- end modal static -->

<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><?php echo date('D, d F Y H:i:s') ?></div>
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center title-2" id="chatTicket" adm="<?php echo base_url(); ?>chatticket?adm=<?php echo $session_admin; ?>">Do The best</h3>
                        </div>
                        <hr>
                        <!-- <form action="" method="post" novalidate="novalidate"> -->
                            <div class="form-group" id="loadChatData"></div>
                            <div class="form-group">
                                <label for="replyTicket" class="control-label mb-1"><h4>Reply</h4></label>
                                <textarea required name="replyTicket" id="replyTicket" rows="5" placeholder="Reply..." class="form-control"></textarea>
                                <span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                        <button id="replyTicketButton" rpl="<?php echo base_url(); ?>chatticket?adm=<?php echo $session_admin; ?>&reply=true" class="btn btn-lg btn-info btn-block">
                                        <i class="fa fa-send fa-lg"></i>&nbsp;
                                        <span id="replyTicketButtonLabel">Send</span>
                                        <span id="replyTicketButtonSending" style="display:none;">Reply…</span>
                                    </button>
                                </div>

                                <div class="col-md-4">
                                    <button type="button" class="btn btn-lg btn-danger btn-block" data-toggle="modal" data-target="#staticModal">
                                            <i class="fa fa-trash fa-lg"></i> Close Ticket
                                    </button>
                                </div>
                            </div>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
