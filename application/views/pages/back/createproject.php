<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Create Project</strong> <small>input project data</small>
                    </div>
                    <div class="card-body card-block">
                        <form action="" method="post" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="ProjectName" class=" form-control-label">Project Name</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input required="" name="ProjectName" id="ProjectName" class="form-control" type="text" placeholder="Project Name" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="DescriptionProject" class=" form-control-label">Description Project</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <textarea required="" name="DescriptionProject" id="DescriptionProject" rows="9" placeholder="Description Project..." class="form-control"></textarea>
                                </div>
                            </div>
                            
                            <div class="card-footer">
                                <button name="CreateProject" type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Create Project
                                </button>
                                <b style="color: red"><?php if(isset($msg)) { echo $msg; } ?></b>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Project PIC</strong> <small>binding project with PIC</small>
                    </div>
                    <div class="card-body card-block">
                        <form action="" method="post" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="SelectEmploye" class=" form-control-label">Select Name</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <select required name="SelectEmploye" id="SelectEmploye" class="form-control">
                                        <option value="0">Please select project</option>
                                        <?php if(isset($users)) { foreach ($users as $key => $value) { ?>
                                            <option value="<?php echo $value->id_employe ?>"><?php echo ucfirst($value->name_employe) ?></option>
                                        <?php } } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="SelectProject" class=" form-control-label">Employe Name</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <select required name="SelectProject" id="SelectProject" class="form-control">
                                        <option value="0">Please select employe</option>
                                        <?php if(isset($projects)) { foreach ($projects as $key => $value) { ?>
                                            <option value="<?php echo $value->id_project ?>"><?php echo ucfirst($value->project_name) ?></option>
                                        <?php } } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="Notes" class=" form-control-label">Notes</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <textarea required name="Notes" id="Notes" rows="9" placeholder="Notes..." class="form-control"></textarea>
                                </div>
                            </div>
                            
                            <div class="card-footer">
                                <button name="BindingProject" type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Binding Project
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
