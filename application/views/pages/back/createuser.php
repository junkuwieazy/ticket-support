<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Add Employe</strong> <small>input employe emial, and system will be send password to email</small>
                    </div>
                    <div class="card-body card-block">
                        <form action="" method="post" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="EmployeName" class=" form-control-label">Employe Name</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input value="<?php echo set_value('EmployeName'); ?>" name="EmployeName" id="EmployeName" class="form-control" type="text" placeholder="Employe Name" required />
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="EmployeEmail" class=" form-control-label">Employe Email</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input value="<?php echo set_value('EmployeEmail'); ?>" name="EmployeEmail" id="EmployeEmail" class="form-control" type="email" placeholder="Employe Email" required />
                                </div>
                            </div>
                            
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Add Employe
                                </button>
                                <b style="color: red"><?php if(isset($msg)) { echo $msg; } ?></b>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
