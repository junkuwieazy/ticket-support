<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row m-t-25">

                <?php if(isset($logfour)) { $style = 1; foreach($logfour as $log) { ?>
                <div class="col-sm-6 col-lg-3">
                    <div class="overview-item overview-item--c<?php echo $style; ?>">
                        <div class="overview__inner">
                            <div class="overview-box clearfix">
                                <div style="margin-bottom: 30px" class="text">
                                    <?php if($log->ticket_status == 'handled') {?>
                                    <h2><?php $dateNew = new DateTime($log->handle_datetime); echo $dateNew->format('d-m-y'); ?></h2>
                                    <span><?php echo ucfirst($log->ticket_status); ?> : <?php echo ucfirst($log->name_employe); ?></span>
                                    <?php } else { ?>
                                        <h2><?php $dateNew = new DateTime($log->close_datetime); echo $dateNew->format('d-m-y'); ?></h2>
                                        <span><?php echo ucfirst($log->ticket_status); ?> : <?php echo ucfirst($log->name_employe); ?></span>
                                    <?php } ?>

                                    <br>
                                    <span><?php echo ucfirst(substr($log->project_name, 0,18)); ?>...</span> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $style++; } } ?>


            </div>
            <div class="row">

                <div class="col-lg-12">
                    <h2 class="title-1 m-b-25">Ticket Logs</h2>
                    <div class="table-responsive table--no-card m-b-40">
                        <table class="table table-borderless table-striped table-earning">
                            <thead>
                                <tr>
                                    <th>open date</th>
                                    <th>handle date</th>
                                    <th>close date</th>
                                    <th>project</th>
                                    <!-- <th class="text-right">price</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($tickets_list)) { $style = 1; foreach($tickets_list as $ticketlog) { ?>
                                <tr>
                                    <td><?php $dateLogOpen = new DateTime($ticketlog->ticket_datetime ); echo $dateLogOpen->format('d-F-y H:i:s'); ?></td>
                                    <td><?php $dateLogHandle = new DateTime($ticketlog->handle_datetime); echo $dateLogHandle->format('d-F-y H:i:s'); ?></td>
                                    <td><?php $dateLogClose = new DateTime($ticketlog->close_datetime); echo $dateLogClose->format('d-F-y H:i:s'); ?></td>
                                    <td><?php echo ucfirst($ticketlog->project_name); ?></td>
                                </tr>
                                <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="title-1 m-b-25">Ticket Chart</h2>
                    <div class="au-card chart-percent-card">
                        <div class="percent-chart">
                            <canvas id="percent-chart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
