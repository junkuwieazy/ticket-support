<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12">
                    <h2 class="title-1 m-b-25">
                        Project List
                        <a href="<?php echo base_url(); ?>createproject"><small>Create Project</small></a>
                    </h2>
                    <div class="table-responsive table--no-card m-b-40">
                        <table class="table table-borderless table-striped table-earning">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Desc</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php if(isset($projects)) { foreach ($projects as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo $value->id_project; ?></td>
                                        <td><?php echo ucfirst($value->project_name); ?></td>
                                        <td><?php echo $value->project_desc; ?></td>
                                    </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>