<?php if(isset($tickets)) { foreach ($tickets as $key => $value) { ?>
<!-- modal static -->
<div class="modal fade" id="staticModal<?php echo $value->id_ticket; ?>" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true"
 data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticModalLabel">Project</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-borderless table-striped table-earning">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Priority</th>
                            <th>From</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo ucfirst($value->project_name); ?></td>
                            <td><?php echo $value->ticket_priority; ?></td>
                            <td><?php echo ucfirst($value->ticket_from_platform); ?></td>
                        </tr>     
                    </tbody>
                </table>
                <br>
                <h4>Description</h4>
                <div id="staticModalDesc">
                    <?php echo $value->ticket_desc; ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <a id="TakeAndHandled" href="<?php echo base_url(); ?>listticket?ticket=<?php echo $value->id_ticket; ?>&handle=true"><button type="button" class="btn btn-primary">Take And Handled</button></a>
            </div>
        </div>
    </div>
</div>
<!-- end modal static -->
<?php } } ?>

<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12">
                    <h2 class="title-1 m-b-25">Ticket List</h2>
                    <div class="table-responsive table--no-card m-b-40">
                        <table class="table table-borderless table-striped table-earning">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Project Name</th>
                                    <th>Priority</th>
                                    <th>From</th>
                                    <th>Desc</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php if(isset($tickets)) { foreach ($tickets as $key => $value) { ?>
                                    <tr>
                                        <td><a data-toggle="modal" data-target="#staticModal<?php echo $value->id_ticket; ?>" t="<?php echo $value->id_ticket; ?>" ticket-url="<?php echo base_url(); ?>listticket?ticket=<?php echo $value->id_ticket; ?>" href="#"><i class="fa fa-eye"></i> <?php echo $value->id_ticket; ?></a></td>
                                        <td><?php echo ucfirst($value->project_name); ?></td>
                                        <td><?php echo $value->ticket_priority; ?></td>
                                        <td><?php echo ucfirst($value->ticket_from_platform); ?></td>
                                        <td><?php echo substr($value->ticket_desc, 0,20); ?> ..</td>
                                    </tr>                                    
                                <?php }} ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>