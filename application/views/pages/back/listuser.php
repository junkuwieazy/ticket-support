<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12">
                    <h2 class="title-1 m-b-25">
                        Employe List
                        <a href="<?php echo base_url(); ?>createuser"><small>Add Employe</small></a>
                    </h2>
                    <div class="table-responsive table--no-card m-b-40">
                        <table class="table table-borderless table-striped table-earning">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Stage</th>
                                    <th>Status</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php if(isset($employes)) { foreach ($employes as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo $value->id_employe; ?></td>
                                        <td><?php echo ucfirst($value->name_employe); ?></td>
                                        <td><?php echo $value->email_employe; ?></td>
                                        <td><?php echo $value->id_employe; ?></td>
                                        <td><?php if($value->employe_status == 1) { echo 'Active'; } else { echo 'Non Active';} ?></td>
                                    </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>