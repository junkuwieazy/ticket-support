<div class="page-wrapper">
    <div class="page-content--bge5">
        <div class="container">
            <div class="login-wrap">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="<?php echo base_url(); ?>">
                            <img src="<?php echo base_url(); ?>assets/images/icon/mobiwin-logo.png" alt="CoolAdmin">
                        </a>
                        <hr>
                        <h5>Muloska Group Employe Signin</h5>
                        <hr>
                    </div>
                    <div class="login-form">
                        <form action="" method="post">
                            <div class="form-group">
                                <label>Work Email Address</label>
                                <input required="" class="au-input au-input--full" type="email" name="WorkEmailAddress" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label>Employe Password</label>
                                <input required="" class="au-input au-input--full" type="password" name="EmployePassword" placeholder="Password">
                            </div>
                            <div class="login-checkbox">
                                <label>
                                    <input type="checkbox" name="remember">Remember Me
                                </label>
                                <label>
                                    <a href="#">Forgotten Password?</a>
                                </label>
                            </div>
                            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Sign In</button>
                            <b style="color: red"><?php if(isset($msg)) { echo $msg; } ?></b>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
