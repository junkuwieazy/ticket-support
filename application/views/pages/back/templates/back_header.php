<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Mobiwin Employes Reporting">
    <meta name="author" content="Juri Pebrianto 2019">
    <meta name="keywords" content="reporting, mobiwin, pebri, employes">

    <!-- Title Page-->
    <title><?php echo $page_title; ?></title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url(); ?>assets/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url(); ?>assets/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url(); ?>assets/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url(); ?>assets/css/theme.css" rel="stylesheet" media="all">

    <!-- Favicon-->
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/icon/favicon.png" />

</head>

<body class="animsition">

    <?php if($dash) { ?>
        <div class="page-wrapper">
            <!-- HEADER MOBILE-->
            <header class="header-mobile d-block d-lg-none">
                <div class="header-mobile__bar">
                    <div class="container-fluid">
                        <div class="header-mobile-inner">
                            <a class="logo" href="index.html">
                                <img src="<?php echo base_url(); ?>assets/images/icon/mobiwin-logo.png" alt="CoolAdmin" />
                            </a>
                            <button class="hamburger hamburger--slider" type="button">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <nav class="navbar-mobile">
                    <div class="container-fluid">
                        <ul class="navbar-mobile__list list-unstyled">

                            <li> <a href="<?php echo base_url(); ?>dashboard"><i class="fas fa-tachometer-alt"></i>Dashboard</a></li>
                            <li> <a href="<?php echo base_url(); ?>listticket"><i class="fas fa-vcard"></i>Ticket</a></li>
                            <?php if($this->session->userdata('admin_session_status') == 2) { ?>
                            <li class="has-sub"><a class="js-arrow" href="#"><i class="fas fa-user"></i>Super User <i class="fa fa-arrow-down"></i></a>
                                <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                    <li> <a href="<?php echo base_url(); ?>createproject"><i class="fas fa-gears"></i>Create Project</a></li>
                                    <li> <a href="<?php echo base_url(); ?>listproject"><i class="fas fa-bars"></i>List Project</a></li>
                                    <li> <a href="<?php echo base_url(); ?>createuser"><i class="fas fa-users"></i>Add Employe</a></li>
                                    <li> <a href="<?php echo base_url(); ?>listuser"><i class="fas fa-bars"></i>List Employe</a></li>
                                </ul>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- END HEADER MOBILE-->

            <!-- MENU SIDEBAR-->
            <aside class="menu-sidebar d-none d-lg-block">
                <div class="logo">
                    <a href="#">
                        <img src="<?php echo base_url(); ?>assets/images/icon/mobiwin-logo.png" alt="Cool Admin" />
                    </a>
                </div>
                <div class="menu-sidebar__content js-scrollbar1">
                    <nav class="navbar-sidebar">
                        <ul class="list-unstyled navbar__list">
                            <li> <a href="<?php echo base_url(); ?>dashboard"><i class="fas fa-tachometer-alt"></i>Dashboard</a></li>
                            <li> <a href="<?php echo base_url(); ?>listticket"><i class="fas fa-vcard"></i>Ticket</a></li>

                            <?php if($this->session->userdata('admin_session_status') == 2) { ?>
                            <li class="has-sub"><a class="js-arrow" href="#"><i class="fas fa-user"></i>Super User <i class="fa fa-arrow-down"></i></a>
                                <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                    <li> <a href="<?php echo base_url(); ?>createproject"><i class="fas fa-gears"></i>Create Project</a></li>
                                    <li> <a href="<?php echo base_url(); ?>listproject"><i class="fas fa-bars"></i>List Project</a></li>
                                    <li> <a href="<?php echo base_url(); ?>createuser"><i class="fas fa-users"></i>Add Employe</a></li>
                                    <li> <a href="<?php echo base_url(); ?>listuser"><i class="fas fa-bars"></i>List Employe</a></li>
                                </ul>
                            </li>
                            <?php } ?>
                        </ul>
                    </nav>
                </div>
            </aside>
            <!-- END MENU SIDEBAR-->

            <!-- PAGE CONTAINER-->
            <div class="page-container">
                <!-- HEADER DESKTOP-->
                <header class="header-desktop">
                    <div class="section__content section__content--p30">
                        <div class="container-fluid">
                            <div class="header-wrap">
                                <form class="form-header" action="" method="POST">
                                    <input class="au-input au-input--xl" type="text" name="search" placeholder="Search for datas &amp; reports..." />
                                    <button class="au-btn--submit" type="submit">
                                        <i class="zmdi zmdi-search"></i>
                                    </button>
                                </form>
                                <div class="header-button">
                                    <div class="account-wrap">
                                        <div class="account-item clearfix js-item-menu">
                                            <div class="image">
                                                <img src="<?php echo base_url(); ?>assets/images/icon/avatar-01.jpg" alt="John Doe" />
                                            </div>
                                            <div class="content">
                                                <a class="js-acc-btn" href="#"><?php echo ucfirst( $session_admin_name); ?></a>
                                            </div>
                                            <div class="account-dropdown js-dropdown">
                                                <div class="info clearfix">
                                                    <div class="image">
                                                        <a href="#">
                                                            <img src="<?php echo base_url(); ?>assets/images/icon/avatar-01.jpg" alt="<?php echo ucfirst( $session_admin_name); ?>" />
                                                        </a>
                                                    </div>
                                                    <div class="content">
                                                        <h5 class="name">
                                                            <a href="#"><?php echo ucfirst($session_admin_name); ?></a>
                                                        </h5>
                                                        <span class="email"><?php echo $session_admin_email; ?></span>
                                                    </div>
                                                </div>
                                                <div class="account-dropdown__body">
                                                    <div class="account-dropdown__footer">
                                                        <a href="<?php echo base_url(); ?>updatepassword"><i class="zmdi zmdi-accounts-list-alt"></i>Change Password</a>
                                                    </div>
                                                </div>
                                                <div class="account-dropdown__body">
                                                    <div class="account-dropdown__footer">
                                                        <a href="<?php echo base_url(); ?>signout"><i class="zmdi zmdi-power"></i>Sign Out</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                    <!-- HEADER DESKTOP-->    

                    <?php } ?>