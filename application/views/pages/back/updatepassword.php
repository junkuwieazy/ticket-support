<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Change Password</strong> <small>input your old password and the new one</small>
                    </div>
                    <div class="card-body card-block">
                        <form action="" method="post" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="OldPassword" class=" form-control-label">Old Password</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input value="<?php echo set_value('OldPassword'); ?>" name="OldPassword" id="OldPassword" class="form-control" type="password" placeholder="Old Password" required />
                                </div>
                            </div>

                            <hr>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="NewPassword" class=" form-control-label">New Password</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input value="<?php echo set_value('NewPassword'); ?>" name="NewPassword" id="NewPassword" class="form-control" type="password" placeholder="New Password" required />
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="ReNewPassword" class=" form-control-label">Re-New Password</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input value="<?php echo set_value('ReNewPassword'); ?>" name="ReNewPassword" id="ReNewPassword" class="form-control" type="password" placeholder="Re-New Password" required />
                                </div>
                            </div>
                            
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-lock"></i> Update Password
                                </button>
                                <b style="color: red"><?php if(isset($msg)) { echo $msg; } ?></b>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
