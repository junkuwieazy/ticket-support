$(document).ready(function() {
	get_chat_all();
});


setInterval(function(){ get_chat_unread(); },3000);


$('#replyTicketButton').click(function(){

	$('#replyTicketButtonSending').show();

	var uReply = $(this).attr('rpl');
	var textReply = $('#replyTicket').val();
	
	if(textReply !== undefined) {
		var uSend = uReply+'&text='+encodeURI(textReply);

		$.ajax({
	        url: uSend,
	        dataType: 'json',
	        success: function(res) {
	        	// console.log(res);

	        	if(res.stat === 0) {
					alert('Empty Reply');
	        	} else {
	        		$('#replyTicket').val('');
	        		$('#replyTicketButtonSending').hide(); 
	        		get_chat_unread();
	        	}
        	},
	        error:function(request, status, error) {
	            console.log("ajax call went wrong : replyTicketButton" + request.responseText);
	        }
	    });
	} else {
		alert('Empty Reply');
	}
});


function get_chat_all() {
	var uAdm = $('#chatTicket').attr('adm');

	$.ajax({
        url: uAdm,
        dataType: 'json',
        success: function(res) {
            var chatDataDetail = '';
            $.each(res, function(index, value){
            	// console.log(uAdm)

            	var colChat = '#6ccf27';
            	if(value.chat_user_role === 'client') {
            		colChat = '#d12104';
            	}

                chatDataDetail += '<label for="Username" class="control-label mb-1"><b style="color: '+colChat+'"><i class="fa fa-user"></i> '+value.chat_user_id+'</b></label>';
                chatDataDetail += '<br>';
                chatDataDetail += '<label for="cc-payment" class="control-label mb-1">';
                chatDataDetail += '<small>'+value.chat_field+'</small>';

                if(value.chat_file !== '') {
                    chatDataDetail += '<br>';
                    chatDataDetail += '<small><a target="_blank" href='+value.chat_file+'>File : <i class="fa fa-download"></i>'+value.chat_filename+'</a></small>';
                }

                chatDataDetail += '</label>';
                chatDataDetail += '<hr>';


                // $('#staticModalLabel').text(value.project_name);
                // $('#staticModalDesc').text(value.ticket_desc);
                // $('#TakeAndHandled').attr("href", uTicket + "&handle=true")
            });

            $("#loadChatData").append(chatDataDetail);

        },
        error:function(request, status, error) {
            console.log("ajax call went wrong get_chat_all :" + request.responseText);
        }
    });
}


function get_chat_unread() {
	var uAdm = $('#chatTicket').attr('adm');

	$.ajax({
        url: uAdm+'&unread=true',
        dataType: 'json',
        success: function(res) {
            var chatDataDetail = '';
            $.each(res, function(index, value){
            	// console.log(value)

            	var colChat = '#6ccf27';
            	if(value.chat_user_role === 'client') {
            		colChat = '#d12104';
            	}

                chatDataDetail += '<label for="Username" class="control-label mb-1"><b style="color: '+colChat+'"><i class="fa fa-user"></i> '+value.chat_user_id+'</b></label>';
                chatDataDetail += '<br>';
                chatDataDetail += '<label for="cc-payment" class="control-label mb-1">';
                chatDataDetail += '<small>'+value.chat_field+'</small>';
                chatDataDetail += '</label>';
                chatDataDetail += '<hr>';


                // $('#staticModalLabel').text(value.project_name);
                // $('#staticModalDesc').text(value.ticket_desc);
                // $('#TakeAndHandled').attr("href", uTicket + "&handle=true")
            });

            $("#loadChatData").append(chatDataDetail);

        },
        error:function(request, status, error) {
            console.log("ajax call went wrong : get_chat_unread" + request.responseText);
        }
    });
}