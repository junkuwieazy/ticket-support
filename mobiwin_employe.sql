/*
SQLyog Ultimate v10.42 
MySQL - 5.5.5-10.1.32-MariaDB : Database - mobiwin_employe
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mobiwin_employe` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mobiwin_employe`;

/*Table structure for table `chat_tb` */

DROP TABLE IF EXISTS `chat_tb`;

CREATE TABLE `chat_tb` (
  `chat_inc` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_chat` varchar(100) DEFAULT NULL,
  `id_ticket` varchar(100) DEFAULT NULL,
  `id_project` varchar(100) DEFAULT NULL,
  `chat_user_role` enum('client','admin') DEFAULT NULL,
  `chat_user_id` varchar(100) DEFAULT NULL,
  `chat_platform` varchar(100) DEFAULT NULL,
  `chat_field` text,
  `chat_read` varchar(10) DEFAULT NULL,
  `chat_file` text,
  `chat_status` varchar(10) DEFAULT NULL,
  `chat_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`chat_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `chat_tb` */

insert  into `chat_tb`(`chat_inc`,`id_chat`,`id_ticket`,`id_project`,`chat_user_role`,`chat_user_id`,`chat_platform`,`chat_field`,`chat_read`,`chat_file`,`chat_status`,`chat_datetime`) values (1,'978684','737477','787985','client','juripebrianto@gmail.com','email','<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','2','files/emails/-79492343-1554436622-des2018.pdf','1','2019-04-05 10:00:10'),(2,'232479','737477','787985','client','juripebrianto@gmail.com','email','<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','2','files/emails/1079028377-1554436622-des2018.pdf','1','2019-04-05 10:00:18'),(3,'792265','737477','787985','client','juripebrianto@gmail.com','email','<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','2','files/emails/646516208-1554436622-des2018.pdf','1','2019-04-05 10:01:04'),(4,'803517','893848','725193','client','882080154','telegram','725193 popopopo','2','','1','2019-04-05 10:11:33');

/*Table structure for table `employe_report_tb` */

DROP TABLE IF EXISTS `employe_report_tb`;

CREATE TABLE `employe_report_tb` (
  `report_inc` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_report` varchar(100) DEFAULT NULL,
  `id_employe` varchar(200) DEFAULT NULL,
  `problem_report` text,
  `report_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`report_inc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employe_report_tb` */

/*Table structure for table `employe_tb` */

DROP TABLE IF EXISTS `employe_tb`;

CREATE TABLE `employe_tb` (
  `employe_inc` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_employe` varchar(100) DEFAULT NULL,
  `name_employe` varchar(200) DEFAULT NULL,
  `email_employe` varchar(200) DEFAULT NULL,
  `password_email` varchar(200) DEFAULT NULL,
  `password_employe` varchar(200) DEFAULT NULL,
  `employe_status` varchar(10) DEFAULT NULL,
  `employe_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`employe_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `employe_tb` */

insert  into `employe_tb`(`employe_inc`,`id_employe`,`name_employe`,`email_employe`,`password_email`,`password_employe`,`employe_status`,`employe_datetime`) values (3,'718675','jati','jati.prasetyo@mobiwin.co.id',NULL,'5f4dcc3b5aa765d61d8327deb882cf99','1','2019-03-27 09:55:54'),(4,'847626','pebri','juri.pebrianto@mobiwin.co.id',NULL,'1de034cf6ea2ca9879cd5a7340cd1db4','1','2019-03-29 03:20:58');

/*Table structure for table `platform_log_tb` */

DROP TABLE IF EXISTS `platform_log_tb`;

CREATE TABLE `platform_log_tb` (
  `log_inc` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_log` varchar(100) DEFAULT NULL,
  `platform` varchar(100) DEFAULT NULL,
  `id_ticket` varchar(100) DEFAULT NULL,
  `log_from` text,
  `log_to` text,
  `log_desc` text,
  `log_desc_option` text,
  `log_response` text,
  `log_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`log_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `platform_log_tb` */

insert  into `platform_log_tb`(`log_inc`,`id_log`,`platform`,`id_ticket`,`log_from`,`log_to`,`log_desc`,`log_desc_option`,`log_response`,`log_datetime`) values (1,'963943','email','414586','tiket@mobiwin.co.id','juripebrianto@gmail.com,juripebrianto@gmail.com,juri.pebrianto@mobiwin.co.id','Someone from your team is talking or in the queue to be handled by our support email','Re: 787985 Masalah Pada Reporting,,,<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','send','2019-04-05 09:47:44'),(2,'959034','email','414586','tiket@mobiwin.co.id','juripebrianto@gmail.com,juripebrianto@gmail.com,juri.pebrianto@mobiwin.co.id','Someone from your team is talking or in the queue to be handled by our support email','Re: 787985 Masalah Pada Reporting,,,<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','send','2019-04-05 09:48:57'),(3,'291945','email','414586','tiket@mobiwin.co.id','juripebrianto@gmail.com,juripebrianto@gmail.com,juri.pebrianto@mobiwin.co.id','Someone from your team is talking or in the queue to be handled by our support email','Re: 787985 Masalah Pada Reporting,,,<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','send','2019-04-05 09:49:13'),(4,'526324','email','414586','tiket@mobiwin.co.id','juripebrianto@gmail.com,juripebrianto@gmail.com,juri.pebrianto@mobiwin.co.id','295335','Re: 787985 Masalah Pada Reporting,,,<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','send','2019-04-05 09:50:33'),(5,'131729','email','414586','tiket@mobiwin.co.id','juripebrianto@gmail.com,juripebrianto@gmail.com,juri.pebrianto@mobiwin.co.id','295335','Re: 787985 Masalah Pada Reporting--,,--<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','send','2019-04-05 09:52:55'),(6,'688459','email','974097','tiket@mobiwin.co.id','juripebrianto@gmail.com,juripebrianto@gmail.com,juri.pebrianto@mobiwin.co.id','Someone from your team is talking or in the queue to be handled by our support emails','Re: 787985 Masalah Pada Reporting--,,--<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','send','2019-04-05 09:54:19'),(7,'484618','email','669986','tiket@mobiwin.co.id','juripebrianto@gmail.com,juripebrianto@gmail.com,juri.pebrianto@mobiwin.co.id','Someone from your team is talking or in the queue to be handled by our support emails','Re: 787985 Masalah Pada Reporting--,,--<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','send','2019-04-05 09:55:41'),(8,'437226','email','527794','tiket@mobiwin.co.id','juripebrianto@gmail.com,juripebrianto@gmail.com,juri.pebrianto@mobiwin.co.id','Thanks, Your ticket number is 527794','Re: 787985 Masalah Pada Reporting--,,--<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','send','2019-04-05 09:59:46'),(9,'796761','email','737477','tiket@mobiwin.co.id','juripebrianto@gmail.com,juripebrianto@gmail.com,juri.pebrianto@mobiwin.co.id','Thanks, Your ticket number is 737477','Re: 787985 Masalah Pada Reporting--,,--<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','send','2019-04-05 10:00:10'),(10,'893254','email','737477','tiket@mobiwin.co.id','juripebrianto@gmail.com,juripebrianto@gmail.com,juri.pebrianto@mobiwin.co.id','335','Re: 787985 Masalah Pada Reporting--,,--<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','send','2019-04-05 10:00:19'),(11,'484143','email','737477','tiket@mobiwin.co.id','juripebrianto@gmail.com,juripebrianto@gmail.com,juri.pebrianto@mobiwin.co.id','335','787985 Masalah Pada Reporting--,,--<div dir=\'auto\'>mau</div><div class=\"gmail_extra\"><br><div class=\"gmail_quote\">On Apr 4, 2019 3:45 PM, Juri Pebriant &lt;juripebrianto@gmail.com&gt; wrote:<br type=\"attribution\" /><blockquote class=\"quote\" style=\"margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex\"><p dir=\"ltr\">Bales lagih</p>\r\n<div><br /><div class=\"elided-text\">On 4 Apr 2019 3:45 p.m.,  &lt;<a href=\"mailto:tiket&#64;mobiwin.co.id\">tiket&#64;mobiwin.co.id</a>&gt; wrote:<br type=\"attribution\" /><blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">On 2019-04-04 15:38, Juri Pebriant wrote:<br />\r\n<blockquote style=\"margin:0 0 0 0.8ex;border-left:1px #ccc solid;padding-left:1ex\">\r\nTolong yawhhhh<br />\r\n</blockquote>\r\n<br />\r\n<br />\r\ntest reply<br />\r\n</blockquote></div></div>\r\n</blockquote></div><br></div>','send','2019-04-05 10:01:04');

/*Table structure for table `project_bind_tb` */

DROP TABLE IF EXISTS `project_bind_tb`;

CREATE TABLE `project_bind_tb` (
  `bind_inc` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_bind` varchar(100) DEFAULT NULL,
  `id_project` varchar(100) DEFAULT NULL,
  `id_employe` varchar(100) DEFAULT NULL,
  `bind_note` text,
  `bind_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`bind_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `project_bind_tb` */

insert  into `project_bind_tb`(`bind_inc`,`id_bind`,`id_project`,`id_employe`,`bind_note`,`bind_datetime`) values (1,'843284','787985','718675','asas','2019-04-02 07:19:03'),(2,'356227','725193','718675','Jati PIC Drupadi','2019-04-04 06:49:37');

/*Table structure for table `project_tb` */

DROP TABLE IF EXISTS `project_tb`;

CREATE TABLE `project_tb` (
  `project_inc` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_project` varchar(100) DEFAULT NULL,
  `project_name` varchar(200) DEFAULT NULL,
  `project_desc` text,
  `project_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`project_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `project_tb` */

insert  into `project_tb`(`project_inc`,`id_project`,`project_name`,`project_desc`,`project_datetime`) values (1,'787985','2020 Dili','CP Telcomcel Dili','2019-04-02 06:59:18'),(2,'725193','92525 - 93535 Drupadi','Layanan 92525 - 93535 Drupadi IP : 103.29.214.114:8103','2019-04-04 06:47:10');

/*Table structure for table `ticket_tb` */

DROP TABLE IF EXISTS `ticket_tb`;

CREATE TABLE `ticket_tb` (
  `ticket_inc` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_ticket` varchar(100) DEFAULT NULL,
  `id_project` varchar(100) DEFAULT NULL,
  `ticket_desc` text,
  `ticket_priority` enum('business_critical','degraded_service','general_issue') DEFAULT NULL,
  `ticket_from_platform` varchar(100) DEFAULT NULL,
  `ticket_from_id` varchar(100) DEFAULT NULL,
  `ticket_status` enum('open','handled','close') DEFAULT NULL,
  `handle_by` varchar(100) DEFAULT NULL,
  `handle_datetime` datetime DEFAULT NULL,
  `close_datetime` datetime DEFAULT NULL,
  `ticket_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`ticket_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `ticket_tb` */

insert  into `ticket_tb`(`ticket_inc`,`id_ticket`,`id_project`,`ticket_desc`,`ticket_priority`,`ticket_from_platform`,`ticket_from_id`,`ticket_status`,`handle_by`,`handle_datetime`,`close_datetime`,`ticket_datetime`) values (1,'737477','787985','Re: 787985 Masalah Pada Reporting','general_issue','email','juripebrianto@gmail.com','open',NULL,NULL,NULL,'2019-04-05 10:00:08'),(2,'893848','725193','725193 popopopo','general_issue','telegram','882080154','open',NULL,NULL,NULL,'2019-04-05 10:11:32');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
